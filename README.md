# (pseudo) Static Site Generator

Because the new version of hugo (0.17) [introduced a bug that broke integration
with remark.js](https://github.com/spf13/hugo/issues/2601), this is my take on a
tool to allow the generation of various [remark.js](https://remarkjs.com/#1)
presentations without having to write a whole bunch of boilerplate for each one.

